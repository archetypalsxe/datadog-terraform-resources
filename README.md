# Datadog Terraform Resources

Collection of Datadog Terraform templates and modules

## Deploying Datadog Account Integration

### PreRequirements
* Terragrunt is required

### Setup
* From the `accountDeployment` directory:
  * Setup Terragrunt Configuration:
    * Copy the file `terragrunt-config-example.yaml` to `terragrunt-config.yaml`
    * Modify the file according to needs
  * Use terragrunt to setup the state bucket/table:
    * `terragrunt init`

### Usage
* `terragrunt plan`
  * See pending changes
* `terragrunt apply`
  * Apply pending changes

## Datadog Documentation Links

* [Necessary IAM Permissions](https://docs.datadoghq.com/integrations/amazon_web_services/)
* [Setting Up API Keys](https://docs.datadoghq.com/account_management/api-app-keys/)
* [Terraform resource](https://registry.terraform.io/providers/DataDog/datadog/latest/docs/resources/integration_aws)
* [General Terraform Steps](https://docs.datadoghq.com/integrations/guide/aws-terraform-setup/)
* [Terraform Provider](https://docs.datadoghq.com/integrations/terraform/#overview)