terraform {
  backend "s3" {}
  required_providers {
    datadog = {
      source = "DataDog/datadog"
    }
  }
}

provider "aws" {
  profile = var.aws_profile
  region  = var.region
  default_tags {
    tags = {
      ProjectName = var.project_name
      UniqueIdentifier = var.unique_identifier
      Owner = "terraform"
    }
  }
}

provider "datadog" {
  api_key = var.data_dog_api_key
  app_key = var.data_dog_app_key
}

module "awsAccountIntegration" {
  region               = var.region
  project_name         = var.project_name
  unique_identifier    = var.unique_identifier
  keys_to_create       =  var.keys_to_create
  source               = "../modules/awsAccountIntegration"
  providers = {
    datadog = datadog
  }
}