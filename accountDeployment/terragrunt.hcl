locals {
  config = yamldecode(file("terragrunt-config.yaml"))
}

inputs = {
  project_name = local.config.project_name
  unique_identifier = local.config.unique_identifier
  region = local.config.aws_region
  aws_profile = local.config.aws_profile
  data_dog_api_key = local.config.data_dog_api_key
  data_dog_app_key = local.config.data_dog_app_key
  keys_to_create = try(local.config.keys_to_create, [])
  working_dir = get_terragrunt_dir()
}

dependencies {
  # This doesn't seem to be necessary
  # paths = ["../modules", "../modules/awsAccountIntegration"]
  paths = []
}

terraform {
  # Removing this seemed to fix an issue where it couldn't find the account integration module
  # source = ".///"

  extra_arguments "env" {
    commands = ["init", "apply", "refresh", "import", "plan", "taint", "untaint"]
  }

  extra_arguments "init_args" {
    commands = [
      "init"
    ]

    # Always treat remote backend state as controlling.
    arguments = [
      "--reconfigure",
    ]
  }
}

remote_state {
  backend = "s3"

  config = {
    region = local.config.aws_region

    bucket  = "${local.config.project_name}-${local.config.unique_identifier}-remote-state"
    key     = "terraform.tfstate"
    encrypt = true

    dynamodb_table = "${local.config.project_name}-${local.config.unique_identifier}-remote-locks"
  }
}