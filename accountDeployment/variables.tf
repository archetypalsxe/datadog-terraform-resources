variable "region" {
  description = "The deploy target region in AWS"
  type = string
}

variable "project_name" {
  description = "Identifier for this project"
  type = string
}

variable "unique_identifier" {
  description = "A unique identifier for this particular deployment"
  type = string
}

variable "working_dir" {
  description = "The location where the Terragrunt config file is"
  type = string
}

variable "aws_profile" {
  description = "The AWS profile we should be using to interact with Terraform"
  type        = string
}

variable "data_dog_api_key" {
  description = "The API Key generated through Data Dog"
  type        = string
  default     = ""
}

variable "data_dog_app_key" {
  description = "The application key generated through Data Dog"
  type        = string
  default     = ""
}

variable "keys_to_create" {
  description = "(Optional) List of application and API keys that should be created and stored in AWS Secrets Manager"
  type        = list(string)
  default     = []
}