resource "datadog_api_key" "api_keys" {
  count = length(var.keys_to_create)
  name = var.keys_to_create[count.index]
}

resource "datadog_application_key" "application_keys" {
  count = length(var.keys_to_create)
  name = var.keys_to_create[count.index]
}

resource "aws_secretsmanager_secret" "datadog_keys" {
  count = length(var.keys_to_create)
  # Make it all lower case with dashes instead of spaces
  name = "datadog-keys-${lower(replace(var.keys_to_create[count.index], " ", "-"))}"
}

resource "aws_secretsmanager_secret_version" "datadog_key_values" {
  count = length(var.keys_to_create)
  secret_id = aws_secretsmanager_secret.datadog_keys[count.index].id
  secret_string = jsonencode({
    "api_key" = datadog_api_key.api_keys[count.index].key
    "app_key" = datadog_application_key.application_keys[count.index].key
  })
}