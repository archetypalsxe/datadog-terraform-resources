variable "region" {
  description = "Region of the AWS VPC"
  type        = string
}

variable "project_name" {
  description = "The name of the project that we are building"
  type        = string
}

variable "unique_identifier" {
  description = "A unique identifier to identify the deployment"
  type        = string
}

variable "keys_to_create" {
  description = "(Optional) List of application and API keys that should be created and stored in AWS Secrets Manager"
  type        = list(string)
  default     = []
}

locals {
  resource_prefix = "${var.project_name}-${var.unique_identifier}-"
}