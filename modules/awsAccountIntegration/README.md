# Documentation Links

* [Necessary IAM Permissions](https://docs.datadoghq.com/integrations/amazon_web_services/)
* [Setting Up API Keys](https://docs.datadoghq.com/account_management/api-app-keys/)
* [Terraform resource](https://registry.terraform.io/providers/DataDog/datadog/latest/docs/resources/integration_aws)
* [General Terraform Steps](https://docs.datadoghq.com/integrations/guide/aws-terraform-setup/)
* [Terraform Provider](https://docs.datadoghq.com/integrations/terraform/#overview)