output datadog_key_secrets {
  description = "List of AWS Secret names that contain Datadog Application and API keys"
  value = "${aws_secretsmanager_secret.datadog_keys.*.name}"
}