variable "errors_enabled" {
  type = bool
  default = true
  description = "Whether or not to enable the errors monitor"
}

variable "invocations_enabled" {
  type = bool
  default = true
  description = "Whether or not to enable the invocations monitor"
}

variable "project_name" {
  description = "Identifier for this project"
  type = string
}

variable "unique_identifier" {
  description = "A unique identifier for this particular deployment"
  type = string
}

variable "filter_tags" {
  description = "List of tags to filter Lambda functions based on"
  type = list(string)
}

variable "alert_emails" {
  description = "List of emails to receive datadog alerts from"
  type = list(string)
  default = []
}

variable "dynamodb_table" {
  description = "The DynamoDB table that we want to monitor"
  type = string
  default = ""
}

variable "dynamodb_size_threshold" {
  description = "The size the DynamoDB table should be before we start alerting"
  type = number
  default = 500
}

locals {
  resource_prefix = "${var.project_name}-${var.unique_identifier}"
  invocations_query = "aws.lambda.invocations{${join(",", var.filter_tags)}}"
  invocations_total_sum = "sum:${local.invocations_query}.as_count()"
  invocations_function_sum = "sum:${local.invocations_query} by {functionname}.as_count()"
  errors_query = "aws.lambda.errors{${join(",", var.filter_tags)}}"
  errors_total_sum = "sum:${local.errors_query}.as_count()"
  errors_function_sum = "sum:${local.errors_query} by {functionname}.as_count()"
  dynamodb_items = "aws.dynamodb.item_count{tablename:${var.dynamodb_table}}"
}

variable "errors_time_aggregator" {
  description = "Monitor aggregator for Errors [available values: min, max or avg]"
  type        = string
  default     = "sum"
}

variable "errors_timeframe" {
  description = "Monitor timeframe for Errors [available values: `last_#m` (1, 5, 10, 15, or 30), `last_#h` (1, 2, or 4), or `last_1d`]"
  type        = string
  default     = "last_1d"
}

variable "errors_threshold_critical" {
  default     = 3
  description = "Alerting threshold in milliseconds"
}

variable "evaluation_delay" {
  description = "Delay in seconds for the metric evaluation"
  default     = 900
}

variable "new_group_delay" {
  description = "Delay in seconds before monitor new resource"
  default     = 300
}

variable "errors_threshold_warning" {
  default     = 1
  description = "Warning threshold in milliseconds"
}

variable "errors_extra_tags" {
  description = "Extra tags for Errors monitor"
  type        = list(string)
  default     = []
}

variable "invocations_time_aggregator" {
  description = "Monitor aggregator for Invocations [available values: min, max or avg]"
  type        = string
  default     = "sum"
}

variable "invocations_timeframe" {
  description = "Monitor timeframe for Invocations [available values: `last_#m` (1, 5, 10, 15, or 30), `last_#h` (1, 2, or 4), or `last_1d`]"
  type        = string
  default     = "last_1d"
}

variable "invocations_threshold_critical" {
  default     = 1000
  description = "Alerting threshold in number of invocations"
}

variable "invocations_threshold_warning" {
  default     = 500
  description = "Warning threshold in number of invocations"
}

variable "invocations_no_data_timeframe" {
  default     = 120
  description = "Timeframe to check before alerting on no data in minutes"
}

variable "notify_no_data" {
  description = "Will raise no data alert if set to true"
  default     = true
}

variable "invocations_extra_tags" {
  description = "Extra tags for Invocations monitor"
  type        = list(string)
  default     = []
}