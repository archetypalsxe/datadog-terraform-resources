resource "datadog_dashboard" "lambda_dashboard" {
  title = "${local.resource_prefix} Lambda Dashboard"
  description = "Custom Lambda dashboard"
  layout_type = "ordered"
  reflow_type = "auto"

  widget {
    alert_graph_definition {
      alert_id = datadog_monitor.errors.id
      title = datadog_monitor.errors.name
      viz_type = "timeseries"
      title_size = 16
      title_align = "left"
    }
  }

  widget {
    timeseries_definition {
      title = "${datadog_monitor.errors.name} Graph"
      legend_columns = [
        "avg",
        "max",
        "min",
        "sum",
        "value",
      ]
      legend_layout  = "auto"
      show_legend    = true
      title_align    = "left"
      title_size     = "16"

      request {
        display_type   = "line"
        on_right_yaxis = false

        formula {
          formula_expression = "errors"
        }

        query {
          metric_query {
            data_source = "metrics"
            name        = "errors"
            query       = local.errors_function_sum
          }
        }

        style {
          line_type  = "solid"
          line_width = "normal"
          palette    = "dog_classic"
        }
      }
    }
  }

  widget {
    toplist_definition {
      title = "${datadog_monitor.errors.name} Top Errors"
      title_align    = "left"
      title_size     = "16"

      request {
        formula {
          formula_expression = "errors"
          limit {
            count = 10
            order = "desc"
          }
        }

        query {
          metric_query {
            aggregator = "sum"
            data_source = "metrics"
            name        = "errors"
            query       = local.errors_function_sum
          }
        }
      }
    }
  }

  widget {
    alert_graph_definition {
      alert_id = datadog_monitor.invocations.id
      title = datadog_monitor.invocations.name
      viz_type = "timeseries"
      title_size = 16
      title_align = "left"
    }
  }

  widget {
    timeseries_definition {
      title = "${datadog_monitor.invocations.name} Graph"
      legend_columns = [
        "avg",
        "max",
        "min",
        "sum",
        "value",
      ]
      legend_layout  = "auto"
      show_legend    = true
      title_align    = "left"
      title_size     = "16"

      request {
        display_type   = "line"
        on_right_yaxis = false

        formula {
          formula_expression = "invocations"
        }

        query {
          metric_query {
            data_source = "metrics"
            name        = "invocations"
            query       = local.invocations_function_sum
          }
        }

        style {
          line_type  = "solid"
          line_width = "normal"
          palette    = "dog_classic"
        }
      }
    }
  }

  widget {
    toplist_definition {
      title = "${datadog_monitor.invocations.name} Top Functions"
      title_align    = "left"
      title_size     = "16"

      request {
        formula {
          formula_expression = "invocations"
          limit {
            count = 10
            order = "desc"
          }
        }

        query {
          metric_query {
            aggregator = "sum"
            data_source = "metrics"
            name        = "invocations"
            query       = local.invocations_function_sum
          }
        }
      }
    }
  }

  widget {
    alert_graph_definition {
      alert_id = datadog_monitor.dynamodb_table.id
      title = datadog_monitor.dynamodb_table.name
      viz_type = "timeseries"
      title_size = 16
      title_align = "left"
    }
  }


  widget {
    timeseries_definition {
      title = "${local.resource_prefix} Items in ${var.dynamodb_table}"
      legend_columns = [
        "avg",
        "max",
        "min",
        "sum",
        "value",
      ]
      legend_layout  = "auto"
      show_legend    = true
      title_align    = "left"
      title_size     = "16"

      request {
        display_type   = "line"
        on_right_yaxis = false

        formula {
          formula_expression = "items"
        }

        query {
          metric_query {
            data_source = "metrics"
            name        = "items"
            query       = "sum:${local.dynamodb_items}"
          }
        }

        style {
          line_type  = "solid"
          line_width = "normal"
          palette    = "dog_classic"
        }
      }
    }
  }

}