terraform {
  required_providers {
    datadog = {
      source = "DataDog/datadog"
    }
  }
}

resource "aws_sns_topic" "datadog_alerts" {
  name = "${local.resource_prefix}-datadog-alerts"
}

resource "aws_sns_topic_subscription" "datadog_alert_subscription" {
  for_each = toset(var.alert_emails)
  topic_arn = aws_sns_topic.datadog_alerts.arn
  protocol = "email"
  endpoint = each.key
}

# https://github.com/claranet/terraform-datadog-monitors
resource "datadog_monitor" "errors" {
  name = "${local.resource_prefix}-lambda-errors"
  type    = "metric alert"
  priority = 1
  message = "${local.resource_prefix}-lambda-errors: We have too many errors @sns-${aws_sns_topic.datadog_alerts.name}"
  renotify_interval = 120
  renotify_statuses = [
    "alert"
  ]

  query = <<EOQ
    ${var.errors_time_aggregator}(${var.errors_timeframe}):
      default(${local.errors_total_sum},0)
      > ${var.errors_threshold_critical}
  EOQ

  evaluation_delay = var.evaluation_delay

  monitor_thresholds {
    critical = var.errors_threshold_critical
    warning  = var.errors_threshold_warning
  }

  notify_no_data      = false
  require_full_window = false
  notify_audit        = false
  timeout_h           = 0
  include_tags        = true

  tags = concat(["type:cloud", "provider:aws", "resource:lambda", "created-by:terraform"], var.errors_extra_tags)
}

resource "datadog_monitor" "invocations" {
  name = "${local.resource_prefix}-lambda-invocations"
  type    = "metric alert"
  priority = 2
  message = "${local.resource_prefix}-lambda-invocations: We have too many invocations @sns-${aws_sns_topic.datadog_alerts.name}"
  renotify_interval = 120
  renotify_statuses = [
    "alert"
  ]

  query = <<EOQ
    ${var.invocations_time_aggregator}(${var.invocations_timeframe}):
      default(${local.invocations_total_sum},0)
      > ${var.invocations_threshold_critical}
  EOQ

  evaluation_delay = var.evaluation_delay

  monitor_thresholds {
    critical = var.invocations_threshold_critical
    warning  = var.invocations_threshold_warning
  }

  notify_no_data      = var.notify_no_data
  no_data_timeframe   = var.invocations_no_data_timeframe
  require_full_window = false
  notify_audit        = false
  timeout_h           = 0
  include_tags        = true

  tags = concat(["type:cloud", "provider:aws", "resource:lambda", "created-by:terraform"], var.invocations_extra_tags)
}

resource "datadog_monitor" "dynamodb_table" {
  name = "${local.resource_prefix}-dynamodb_size"
  type    = "metric alert"
  priority = 2
  message = "${local.resource_prefix}-dynamodb_size: We have too many items in the table ${var.dynamodb_table} @sns-${aws_sns_topic.datadog_alerts.name}"
  renotify_interval = 120
  renotify_statuses = [
    "alert"
  ]

  query = <<EOQ
    max(${var.errors_timeframe}):
      default(max:${local.dynamodb_items},0)
      > ${var.dynamodb_size_threshold}
  EOQ

  evaluation_delay = var.evaluation_delay

  monitor_thresholds {
    critical = var.dynamodb_size_threshold
  }

  notify_no_data      = false
  require_full_window = false
  notify_audit        = false
  timeout_h           = 0
  include_tags        = true

  tags = concat(["type:cloud", "provider:aws", "resource:lambda", "created-by:terraform"], var.errors_extra_tags)
}